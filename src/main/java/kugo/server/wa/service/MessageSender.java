package kugo.server.wa.service;

import kugo.server.wa.component.HttpUtil;
import kugo.server.wa.component.MqUtil;
import kugo.server.wa.component.MsgUtil;
import org.apache.http.client.config.RequestConfig;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.Session;

import java.lang.invoke.MethodHandles;

import static kugo.server.wa.config.ActiveMq.QUEUE_INBOUND_MSG;
import static kugo.server.wa.config.ActiveMq.QUEUE_SEND_MSG;

@Component
public class MessageSender {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MsgUtil msgUtil;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    MqUtil mqUtil;
    @Autowired
    HttpUtil httpUtil;
    @Value("${my.allow-send-message}")
    boolean allowSendMessage;

    @JmsListener(destination = QUEUE_SEND_MSG)
    public void receiveMessage(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        JSONObject jsonRawMsg = new JSONObject();
        try {
            jsonRawMsg = new JSONObject(jsonObjectString);
            JSONObject jsonReq = jsonRawMsg.getJSONObject("jsonReq");

            log.info("Sending request received: "+jsonRawMsg);
            while (asyncExecutor.getActiveCount() > 10) {
                Thread.sleep(3000);
                log.info("Waiting for free thread ...");
            }

            asyncExecutor.execute(new MessageSenderWorker(jsonRawMsg.getLong("id"), jsonReq));
        }
        catch (org.springframework.core.task.TaskRejectedException e ) {
            mqUtil.send(QUEUE_SEND_MSG, jsonRawMsg);
        } catch (Exception e) {
            log.error("Error receiving messsage.."+e,e);
        }
    }

    private class MessageSenderWorker implements Runnable {
        private final JSONObject jsonReq;
        private final long id;

        public MessageSenderWorker(long id, JSONObject jsonReq) {
            this.jsonReq = jsonReq;
            this.id = id;
        }

        @Override
        public void run() {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("id", id);

            if (!allowSendMessage) {
                log.info("Send message is disabled!");
                return;
            }

            try {
                String url = msgUtil.getConfig("infobip.base.url")+"/omni/1/advanced";

                JSONObject jsonResp = httpUtil.postJson(url, jsonReq);
                log.info("JSON Resp: "+jsonResp.toString());

                JSONObject jsonMsgResp = jsonResp.getJSONArray("messages").getJSONObject(0);
                params.addValue("sentId", jsonMsgResp.getString("messageId"));
                params.addValue("sentStatus", jsonMsgResp.getJSONObject("status").getString("name"));
                params.addValue("sentDescription", jsonMsgResp.getJSONObject("status").getString("description"));

            } catch (Exception e) {
                log.error("Error while sending JSON: "+e,e);
                params.addValue("sentId", null);
                params.addValue("sentStatus", "FAILED");
                params.addValue("sentDescription", ""+e);
            }
            String sql = " UPDATE wa_msg_out SET sent_on=NOW(),sent_id=:sentId, sent_status=:sentStatus, sent_description=:sentDescription " +
                    " WHERE id=:id ";
            jdt.update(sql, params);
        }
    }
}
