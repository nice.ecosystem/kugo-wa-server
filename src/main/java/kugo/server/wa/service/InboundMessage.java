package kugo.server.wa.service;

import kugo.server.wa.component.MqUtil;
import kugo.server.wa.component.MsgUtil;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.Session;

import java.lang.invoke.MethodHandles;

import static kugo.server.wa.config.ActiveMq.*;

@Component
public class InboundMessage {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MqUtil mqUtil;
    @Autowired
    MsgUtil msgUtil;

    JSONObject jsonRawMsg;

    @JmsListener(destination = QUEUE_INBOUND_MSG)
    public void receiveMessage(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            jsonRawMsg = new JSONObject(jsonObjectString);
            storeMsg(jsonRawMsg);
        } catch (Exception e) {
            log.error("Error receiving messsage.."+e,e);
        }
    }

    private void storeMsg(JSONObject jsonRawMsg) throws Exception {

        JSONArray jsonMsgs = jsonRawMsg.getJSONArray("results");
        for (int i=0;i<jsonMsgs.length();i++) {
            JSONObject jsonMsg = jsonMsgs.getJSONObject(i);

            if (jsonMsg.has("message")) processMessage(jsonMsg);
            else if (jsonMsg.has("status")) processStatus(jsonMsg);
            else if (jsonMsg.has("seenAt")) processSeen(jsonMsg);
        }
    }

    private void processStatus(JSONObject jsonMsg) throws Exception {
        String msgId = jsonMsg.getString("messageId");
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("sentStatus", jsonMsg.getJSONObject("status").getString("name"));
        params.addValue("sentDescription", jsonMsg.getJSONObject("status").getString("description"));
        params.addValue("sentId", msgId);

        String sql = " UPDATE wa_msg_out SET sent_status=:sentStatus, sent_description=:sentDescription WHERE sent_id=:sentId ";
        jdt.update(sql, params);
    }

    private void processSeen(JSONObject jsonMsg) throws Exception {
        DateTimeFormatter parser    = ISODateTimeFormat.dateTimeParser();
        String msgId = jsonMsg.getString("messageId");
        DateTime dt = parser.parseDateTime(jsonMsg.getString("seenAt"));
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("seenOn", dt.toDate());
        params.addValue("sentId", msgId);

        String sql = " UPDATE wa_msg_out SET seen_on=:seenOn WHERE sent_id=:sentId ";
        jdt.update(sql, params);
    }

    private void processMessage(JSONObject jsonMsg) throws Exception {
        String msgId = jsonMsg.getString("messageId");

        JSONObject jsonMsgBody = jsonMsg.getJSONObject("message");
        String msgType = jsonMsgBody.getString("type");

        String mobile = msgUtil.reformatMobile(jsonMsg.getString("from"));
        String dest = msgUtil.reformatMobile(jsonMsg.getString("to"));

        String msgText = null;
        String msgUrl = null;
        String queueProcess = null;

        if (msgType.equals(MsgUtil.MSG_TYPE_TEXT)) {
            queueProcess = QUEUE_INBOUND_MSG_TEXT;
            msgText = jsonMsgBody.getString("text");
        } else if (msgType.equals(MsgUtil.MSG_TYPE_IMAGE) || msgType.equals(MsgUtil.MSG_TYPE_DOCUMENT)) {
            msgText = (jsonMsgBody.has("caption"))?jsonMsgBody.getString("caption"):null;
            msgUrl = jsonMsgBody.getString("url");
        } else if (msgType.equals(MsgUtil.MSG_TYPE_LOCATION)) {
            msgText = jsonMsgBody.getString("longitude")+","+jsonMsgBody.getString("latitude");
        }

        log.info("Storing msg ID: "+msgId);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("msgId", msgId);
        params.addValue("mobile", mobile);
        params.addValue("dest", dest);
        params.addValue("msgType", msgType);
        params.addValue("msgText", msgText);
        params.addValue("msgUrl", msgUrl);

        KeyHolder holder = new GeneratedKeyHolder();
        String sql = " INSERT INTO wa_msg_in (msg_id,mobile,dest,msg_type,msg_text,msg_url) VALUES " +
                " (:msgId,:mobile,:dest,:msgType,:msgText,:msgUrl) ";
        jdt.update(sql, params, holder);

        long id = holder.getKey().longValue();

        JSONObject jsonStoredMsg = new JSONObject(params.getValues());
        jsonStoredMsg.put("id", id);
        jsonStoredMsg.put("oriReqBody", jsonRawMsg.toString());

        if (queueProcess!=null)
            mqUtil.send(queueProcess, jsonStoredMsg);
    }

}
