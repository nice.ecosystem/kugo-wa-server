package kugo.server.wa.service;

import kugo.server.wa.component.HttpUtil;
import kugo.server.wa.component.MsgUtil;
import kugo.server.wa.component.activity.BaseActivity;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import javax.jms.Message;
import javax.jms.Session;
import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import static kugo.server.wa.config.ActiveMq.QUEUE_INBOUND_MSG_TEXT;

@Component
public class InboundMessageText {

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MsgUtil msgUtil;
    @Autowired
    ApplicationContext context;
    @Autowired
    HttpUtil httpUtil;

    @JmsListener(destination = QUEUE_INBOUND_MSG_TEXT)
    public void receiveMessage(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            JSONObject moMsg = new JSONObject(jsonObjectString);
            log.info("Processing message: "+moMsg);

            String moText = moMsg.getString("msgText").trim();
            if (moText.isEmpty()) return;

            String keyword = (moText.split(" "))[0].toUpperCase();
            log.info("Keyword: "+keyword);
            moMsg.put("keyword", keyword);
            JSONObject rootActivity = getRootActivity(keyword);
            if (rootActivity!=null) {
                startNewSession(moMsg, rootActivity);
                return;
            }
            else continueSession(moMsg);
            //msgUtil.sendText(jsonRawMsg.getString("mobile"),"You just texted me \""+jsonRawMsg.getString("msgText")+"\"?");
            //String imgUrl = "https://cdn0-production-images-kly.akamaized.net/4KHu9qdSv0WHtwbOUUe6QuZ08K4=/640x640/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/872598/original/088472300_1431113802-Gadis-Golf.jpg";
            //msgUtil.sendImage(jsonRawMsg.getString("mobile"), "You just texted me \""+jsonRawMsg.getString("msgText")+"\"?", imgUrl);
        } catch (Exception e) {
            log.error("Error receiving messsage.."+e,e);
        }
    }

    private void continueSession(JSONObject moMsg) throws Exception {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("mobile", moMsg.getString("mobile"));

        String sql = " SELECT s.*,bean_name,method_name,a.next_activity FROM wa_msg_session s JOIN wa_msg_activity a ON (s.next_activity_id=a.id) " +
                " WHERE mobile=:mobile AND exp_on>=NOW() AND next_activity_id IS NOT NULL AND s.completed=0 " +
                " ORDER BY id DESC LIMIT 0,1 ";
        List<Map<String,Object>> rows = jdt.queryForList(sql, params);
        if (rows.size()<=0) {
            log.info("No available session! Discard the message!");
            //msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(111));
            processJunkMessage(moMsg);
            return;
        }

        JSONObject jsonSession = new JSONObject(rows.get(0));
        try {
            log.info("Found active session#: "+jsonSession.get("id"));
            String beanName = jsonSession.getString("bean_name");
            String methodName = jsonSession.getString("method_name");
            moMsg.put("memberId", jsonSession.get("member_id"));
            moMsg.put("sessionId", jsonSession.get("id"));
            moMsg.put("activityId", jsonSession.get("next_activity_id"));
            moMsg.put("nextActivityId", (jsonSession.has("next_activity"))?jsonSession.get("next_activity"):null);
            moMsg.put("param1", (jsonSession.has("param1"))?jsonSession.get("param1"):null);
            moMsg.put("param2", (jsonSession.has("param2"))?jsonSession.get("param2"):null);
            moMsg.put("param3", (jsonSession.has("param3"))?jsonSession.get("param3"):null);
            invokeMethod(beanName, methodName, moMsg);

        } catch (InvocationTargetException e) {
            Throwable t = e.getCause();
            log.error("Error on calling method: "+t,t);
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(110));
        } catch (Exception e) {
            log.error("Error invoking process .... "+e,e);
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(110));
        }
    }

    private JSONObject getRootActivity(final String keyword) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("keyword", keyword);
        String sql = " SELECT a.* FROM wa_msg_activity_keyword k JOIN wa_msg_activity a ON (a.id=k.activity_id) WHERE k.keyword=:keyword ";
        try {
            JSONObject jsonResp = new JSONObject(jdt.queryForMap(sql, params));
            return jsonResp;
        } catch (Exception e) {
            return null;
        }
    }

    private void startNewSession(JSONObject moMsg, JSONObject rootActivity) throws Exception {
        log.info("Starting new session for activity "+rootActivity.toString());

        try {
            long memberId = getMemberId(moMsg);
            moMsg.put("memberId", memberId);
            log.info("Member ID: #" + memberId);

            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("memberId", memberId);
            params.addValue("mobile", moMsg.getString("mobile"));
            params.addValue("activityId", rootActivity.get("id"));
            params.addValue("nextActivityId", (rootActivity.has("next_activity")) ? rootActivity.get("next_activity") : null);
            params.addValue("serviceId", rootActivity.get("service_id"));

            String sql = " UPDATE wa_msg_session SET completed=1 WHERE mobile=:mobile ";
            jdt.update(sql, params);

            KeyHolder keyHolder = new GeneratedKeyHolder();
            sql = " INSERT INTO wa_msg_session (member_id,mobile,activity_id,next_activity_id,service_id,exp_on)" +
                    " VALUES (:memberId,:mobile,:activityId,:nextActivityId,:serviceId,ADDDATE(NOW(), INTERVAL 60 MINUTE)) ";
            jdt.update(sql, params, keyHolder);

            Long sessionId = keyHolder.getKey().longValue();

            moMsg.put("sessionId", sessionId);
            moMsg.put("activityId", rootActivity.get("id"));
            moMsg.put("nextActivityId", (rootActivity.has("next_activity")) ? rootActivity.get("next_activity") : null);

            String beanName = rootActivity.getString("bean_name");
            String methodName = rootActivity.getString("method_name");
            invokeMethod(beanName, methodName, moMsg);
        }
        catch (InvocationTargetException e) {
            Throwable t = e.getCause();
            log.error("Error on calling method: "+t,t);
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(110));
        } catch (Exception e) {
            log.error("Error invoking process .... "+e,e);
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(110));
        }
    }

    private void invokeMethod(String beanName, String methodName, JSONObject moMsg) throws Exception {
        log.info("Bean: "+beanName+", method: "+methodName);
        Object o = context.getBean(beanName);
        Method method = o.getClass().getMethod(methodName, JSONObject.class);
        method.invoke(o, moMsg);
    }

    private Long getMemberId(JSONObject moMsg) throws Exception {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("mobile", moMsg.getString("mobile"));
        params.addValue("channelId", 3);

        String sql = " SELECT member_id FROM member WHERE mobile=:mobile LIMIT 0,1 ";
        List<Long> rows = jdt.queryForList(sql, params, Long.class);
        if (rows.size() > 0) return rows.get(0);

        KeyHolder holder = new GeneratedKeyHolder();
        sql = " INSERT INTO member (channel_id,mobile) VALUES (:channelId,:mobile) ";
        jdt.update(sql, params, holder);
        return holder.getKey().longValue();
    }

    private void processJunkMessage(JSONObject moMsg) throws Exception {
        final String mobile = moMsg.getString("mobile");
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("mobile", mobile);
        String sql = " SELECT COUNT(*) as ct FROM wa_msg_junk WHERE mobile=:mobile ";
        int count = jdt.queryForObject(sql, params, Integer.class);
        if (count <= 0) {
            sql = " INSERT INTO wa_msg_junk (mobile) VALUES (:mobile) ";
            jdt.update(sql, params);
            msgUtil.sendText(mobile, msgUtil.getMsgText(111));
        } else {
            // throw to3rd party
            String oriReqBody = moMsg.getString("oriReqBody");
            String url = "https://webhook.botika.online/oawhatsapp/oawhatsapp.php";
            httpUtil.postString(url, oriReqBody, "application/json");
        }
    }
}
