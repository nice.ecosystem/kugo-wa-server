package kugo.server.wa.controller;

import kugo.server.wa.component.MqUtil;
import org.apache.commons.io.IOUtils;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;

import static kugo.server.wa.config.ActiveMq.QUEUE_INBOUND_MSG;
import static kugo.server.wa.config.ActiveMq.QUEUE_SEND_MSG;

@RestController
public class RestApi {

    @Autowired
    MqUtil mqUtil;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @RequestMapping(value = {"/message"}, method = RequestMethod.POST, produces = "application/json;charset=UTF-8")
    public String post(HttpServletRequest request) {
        final JSONObject jsonObject = new JSONObject();
        try {
            String reqBody = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8);
            try {
                JSONObject moJson = new JSONObject(reqBody);
                log.info("Req body: "+moJson.toString());
                mqUtil.send(QUEUE_INBOUND_MSG, moJson);
            } catch (JSONException e) {
                log.info("Not JSON message: "+reqBody);
            }
        } catch (Exception e) {

        }
        return jsonObject.toString(4);
    }

    @RequestMapping(value = {"/message"}, method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    public String get(HttpServletRequest request) {
        final JSONObject jsonObject = new JSONObject();
        try {
            Enumeration<String> reqParams = request.getParameterNames();
            while (reqParams.hasMoreElements()) {
                String paramName = reqParams.nextElement();
                log.info("GET param "+paramName+": "+request.getParameter(paramName));
            }
        } catch (Exception e) {

        }
        return jsonObject.toString(4);
    }
}
