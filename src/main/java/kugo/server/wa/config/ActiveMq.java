package kugo.server.wa.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@Configuration
public class ActiveMq {
    public final static String QUEUE_SEND_MSG = "queue-send-message";
    public final static String QUEUE_INBOUND_MSG = "queue-inbound-message";
    public final static String QUEUE_INBOUND_MSG_TEXT = "queue-inbound-message-text";

    public final static String QUEUE_SEND_UPDATE = "queue-send-updater";
}
