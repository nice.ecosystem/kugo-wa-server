package kugo.server.wa.component;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

@Component
public class MqUtil {
    @Autowired
    JmsTemplate jmsTemplate;

    public void send(String queueName, final JSONObject jsonMessage) {
        MessageCreator msgCreator = new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                return session.createTextMessage(jsonMessage.toString());
            }
        };
        jmsTemplate.send(queueName, msgCreator);
    }
}
