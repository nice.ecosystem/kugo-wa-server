package kugo.server.wa.component;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;

@Component
public class HttpUtil {

    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MsgUtil msgUtil;
    @Value("${cx.url}")
    String cxUrl;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    private static final RequestConfig reqConfig = RequestConfig.custom()
            .setConnectTimeout(30000)
            .setConnectionRequestTimeout(30000)
            .setSocketTimeout(30000).build();

    public String postString(String url, String reqBody, String contentType) throws Exception {
        log.debug("Post to URL: "+url+", body:"+reqBody.toString());

        int timeout = 30;
        CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(reqConfig).build();

        String authString = "Basic "+ Base64.encodeBase64String((msgUtil.getConfig("infobip.username")+":"+msgUtil.getConfig("infobip.password")).getBytes());

        HttpPost httpPost = new HttpPost(url);
        StringEntity entity = new StringEntity(reqBody.toString());
        httpPost.setEntity(entity);
        httpPost.setHeader("Authorization", authString);
        //httpPost.setHeader("Accept", contentType);
        httpPost.setHeader("Content-type", contentType);

        CloseableHttpResponse response = client.execute(httpPost);
        String respText = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);

        log.debug("Response status code: "+response.getStatusLine().getStatusCode()+", status text: "+response.getStatusLine().getReasonPhrase());

        client.close();

        return respText;
    }

    public JSONObject postJson(String url, JSONObject jsonReq) throws Exception {
        String reqBody = jsonReq.toString();
        String respText = postString(url, reqBody, "application/json");
        return new JSONObject(respText);
    }

    public JSONObject postToCx(String xml) throws Exception {
        log.debug("Post to URL: "+cxUrl);

        int timeout = 30;

        CloseableHttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(reqConfig).build();

        String authString = "Basic "+ Base64.encodeBase64String((msgUtil.getConfig("infobip.username")+":"+msgUtil.getConfig("infobip.password")).getBytes());

        HttpPost httpPost = new HttpPost(cxUrl);
        StringEntity entity = new StringEntity(xml);
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "text/xml");

        CloseableHttpResponse response = client.execute(httpPost);
        String respText = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8);


        JSONObject jsonResult = XML.toJSONObject(respText);
        log.info("Response: "+jsonResult.toString());

        client.close();

        return jsonResult;
    }


}
