package kugo.server.wa.component;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.text.SimpleDateFormat;
import java.util.Date;

import static kugo.server.wa.config.ActiveMq.QUEUE_SEND_MSG;

@Component
public class MsgUtil {
    public static final String MSG_TYPE_TEXT = "TEXT";
    public static final String MSG_TYPE_IMAGE = "IMAGE";
    public static final String MSG_TYPE_DOCUMENT = "DOCUMENT";
    public static final String MSG_TYPE_LOCATION = "LOCATION";

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MqUtil mqUtil;

    public JSONObject addSendLog(String mobileRaw, String text, String imageUrl) throws Exception {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("mobile", reformatMobile(mobileRaw, false));
        params.addValue("msgText", text);
        params.addValue("msgType", MsgUtil.MSG_TYPE_TEXT);
        params.addValue("msgUrl", imageUrl);

        KeyHolder holder = new GeneratedKeyHolder();
        String sql = " INSERT INTO wa_msg_out (mobile,msg_type,msg_text,msg_url) VALUES " +
                " (:mobile,:msgType,:msgText,:msgUrl) ";
        jdt.update(sql, params, holder);
        long id = holder.getKey().longValue();
        JSONObject jsonMsg = new JSONObject(params.getValues());
        jsonMsg.put("id", id);
        return jsonMsg;
    }

    public void sendText(String mobileRaw, String text) throws Exception {
        JSONObject jsonMsg = addSendLog(mobileRaw, text, null);
        jsonMsg.put("jsonReq", generateMessageText(jsonMsg));
        mqUtil.send(QUEUE_SEND_MSG, jsonMsg);
    }

    public void sendImage(String mobileRaw, String text, String imageUrl) throws Exception {
        JSONObject jsonMsg = addSendLog(mobileRaw, text, imageUrl);
        jsonMsg.put("jsonReq", generateMessageImage(jsonMsg));
        mqUtil.send(QUEUE_SEND_MSG, jsonMsg);
    }

    public String getConfig(String key) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("key",key);

        try {
            String sql = " SELECT config_value FROM config WHERE config_key=:key ";
            return jdt.queryForObject(sql, params, String.class);
        } catch (Exception e) {
            return null;
        }
    }

    public String reformatDate(String rawDate) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(df.parse(rawDate));
    }

    public Date parseDate(String rawDate) throws Exception {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.parse(rawDate);
    }

    public String reformatMobile(String mobileRaw, boolean removePlus) throws Exception {
        String mobile = mobileRaw.replaceFirst("^\\+","");

        return ((removePlus)?"":"+")+mobile.trim();
    }

    public String reformatMobile(String mobileRaw) throws Exception {
        return reformatMobile(mobileRaw, false);
    }

    public JSONObject generateMessageText(JSONObject jsonMsg) throws Exception {
        JSONObject jsonReq = new JSONObject();
        String mobile = reformatMobile(jsonMsg.getString("mobile"), true);
        jsonReq.put("scenarioKey", getConfig("infobip.wa.key"));
        jsonReq.put("destinations", new JSONArray().put(new JSONObject().put("to",new JSONObject().put("phoneNumber", mobile))));
        jsonReq.put("whatsApp", new JSONObject().put("text",jsonMsg.getString("msgText")));
        return jsonReq;
    }

    public JSONObject generateMessageImage(JSONObject jsonMsg) {
        JSONObject jsonReq = new JSONObject();
        jsonReq.put("scenarioKey", getConfig("infobip.wa.key"));
        jsonReq.put("destinations", new JSONArray().put(new JSONObject().put("to",new JSONObject().put("phoneNumber", jsonMsg.getString("mobile")))));
        jsonReq.put("whatsApp", new JSONObject().put("text",jsonMsg.getString("msgText")).put("imageUrl", jsonMsg.getString("msgUrl")));
        return jsonReq;
    }

    public String getMsgText(int textId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("textId", textId);

        String sql = " SELECT content FROM wa_msg_text WHERE id=:textId ";
        try {
            return jdt.queryForObject(sql, params, String.class);
        } catch (Exception e) {
            log.error("Text id #"+textId+" is not found!");
            return null;
        }
    }
}
