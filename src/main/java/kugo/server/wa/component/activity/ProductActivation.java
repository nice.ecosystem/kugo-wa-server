package kugo.server.wa.component.activity;

import kugo.server.wa.component.HttpUtil;
import kugo.server.wa.component.MsgUtil;
import org.apache.commons.text.StringSubstitutor;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.text.NumberFormat;
import java.util.*;

@Component
public class ProductActivation extends BaseActivity {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MsgUtil msgUtil;
    @Autowired
    ApplicationContext context;
    @Autowired
    ActivityUtil activityUtil;
    @Autowired
    HttpUtil httpUtil;

    private JSONObject moMsg;

    private final static int PRODUCT_CATEGORY = 101;
    private final static ArrayList<String> productLables = new ArrayList<>();

    static {
        char ch;
        for (ch='A';ch<='Z';ch++) productLables.add(""+ch);
    }

    public void init(JSONObject moMsg) throws Exception {
        log.info("Init Product Activation! request: "+moMsg);
        parseMoMsg(moMsg);

        this.moMsg = moMsg;
        String replyText = msgUtil.getMsgText(201);
        msgUtil.sendText(moMsg.getString("mobile"), replyText);
    }

    public void validateStb(JSONObject moMsg) throws Exception {
        log.info("Validating STB: "+moMsg);
        parseMoMsg(moMsg);
        this.moMsg = moMsg;

        try {

            JSONObject stb = activityUtil.validateStb(keyword, memberId);

            activityUtil.updateSessionParam(sessionId, 1, stb.getString("qr"));

            String msgTemplate = msgUtil.getMsgText(203);
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("categoryId", PRODUCT_CATEGORY);
            String sql = " SELECT product_id, `name`, param1, param2,param3,image_url,price FROM wallet_product WHERE category_id=:categoryId AND is_active=1 AND is_available=1 " +
                    " AND price=0 " +
                    " ORDER BY display_order ";

            List<Map<String,Object>> rows = jdt.queryForList(sql, params);

            if (rows.size()<=0) {
                log.info("No product available!");
                msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(204));
                return;
            }

            JSONObject jsonProductList = new JSONObject();
            log.info("Product list: "+jsonProductList);


            String productList = "";
            char label = 'A';
            for (Map<String,Object> row : rows) {
                jsonProductList.put(""+label, row);
                long price = (long) row.get("price");
                NumberFormat format = NumberFormat.getNumberInstance(Locale.forLanguageTag("in"));

                productList+= "*"+label+"*. "+row.get("name")+((row.get("param2")!=null && !row.get("param2").toString().isEmpty())?" ("+row.get("param2")+" hari)\n":"\n");
                jsonProductList.put(""+label, new JSONObject(row));
                label++;

            }

            if (rows.size() > 1) {
                productList+= "*"+label+"*. Semua paket";
                jsonProductList.put(""+label, new JSONObject().put("name","Semua paket").put("all", true));
            }

            activityUtil.updateSessionParam(sessionId, 2, jsonProductList.toString());

            log.info("Product List: "+productList);
            activityUtil.gotoNextActivity(sessionId, activityId, nextActivityId);

            Map<String,String> replacement = new HashMap<>();
            replacement.put("product-list", productList.trim());
            String replyText = StringSubstitutor.replace(msgTemplate, replacement);

            //activityUtil.gotoNextActivity(sessionId, activityId, nextActivityId);
            msgUtil.sendText(moMsg.getString("mobile"), replyText);
        } catch (Exception e) {
            log.error("STB not found!");
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(202));
        }
    }

    public void confirm(JSONObject moMsg) throws Exception {
        log.info("Confirming activation product");
        parseMoMsg(moMsg);

        JSONObject jsonProductList = new JSONObject(param2);

        if (!jsonProductList.has(keyword)) {
            log.info("Option was unknown \""+keyword+"\"!");

            String msgTemplate = msgUtil.getMsgText(205);
            Iterator<String> keys = jsonProductList.keys();
            String productList = "";
            while (keys.hasNext()) {
                String key = keys.next();
                productList+= "*"+key+"*. "+jsonProductList.getJSONObject(key).getString("name")+"\n";
            }
            Map<String,String> replacement = new HashMap<>();
            replacement.put("product-list", productList.trim());
            String replyText = StringSubstitutor.replace(msgTemplate, replacement);
            //activityUtil.gotoNextActivity(sessionId, activityId, nextActivityId);
            msgUtil.sendText(moMsg.getString("mobile"), replyText);

            return;
        }

        JSONObject jsonProduct = jsonProductList.getJSONObject(keyword);
        if (jsonProduct.getInt("price") > 0) {
            msgUtil.sendText(moMsg.getString("mobile"), "Paket siaran '"+jsonProduct.getString("name")+"' adalah paket berbayar, untuk sementara paket berbayar hanya bisa dilakukan melalui *Kugo App*\nSilahkan download disini\nhttp://bit.do/kugoapp");
            return;
        }

        activityUtil.updateSessionParam(sessionId, 3, keyword);
        activityUtil.gotoNextActivity(sessionId, activityId, nextActivityId);
        msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(207));
    }

    public void activate(JSONObject moMsg) throws Exception {
        log.info("Activating product STB: "+moMsg);
        parseMoMsg(moMsg);
        this.moMsg = moMsg;

        if (!keyword.equals("YA")) {
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(208));
            return;
        }
        msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(211));

        JSONObject jsonProductList = new JSONObject(param2);

        ArrayList<String> chosenProducts = new ArrayList<>();
        JSONObject jsonChosen = jsonProductList.getJSONObject(param3);
        if (jsonChosen.has("all")) {
            Iterator<String> keys = jsonProductList.keys();
            String productList = "";
            while (keys.hasNext()) {
                String key = keys.next();
                if (!param3.equals(key)) chosenProducts.add(key);
            }
        } else {
            chosenProducts.add(param3);
        }

        JSONObject stb = activityUtil.getStbDetail(param1);

        for (String key: chosenProducts) {
            activateProduct(stb, jsonProductList.getJSONObject(key));
        }

        activityUtil.finishTheSession(sessionId);
    }

    private void activateProduct(JSONObject stb, JSONObject product) throws Exception {
        log.info("Activating product "+product.toString()+" to STB: "+stb.get("chip_id"));

        boolean refresh = false;

        int durationDays = 30;
        DateTime endTime = DateTime.now().plusDays(durationDays+1);
        if (product.has("param3") && !product.getString("param3").trim().isEmpty()) {
            endTime = dateTimeFormatter.parseDateTime(product.getString("param3"));
        } else {
            durationDays = Integer.parseInt(product.getString("param2"));
            endTime = DateTime.now().plusDays(durationDays+1);
        }
        DateTime beginTime  = DateTime.now();;

        // check if still have active subscription ...
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("qr", stb.get("qr"));
        params.addValue("productName", product.getString("name"));
        params.addValue("packageId", product.get("param1"));

        String sql = " SELECT finish_on, :productName as name, :qr as qr, DATE_FORMAT(finish_on, '%d/%m/%Y') as endDate, DATE_FORMAT(SUBDATE(finish_on, INTERVAL 5 DAY),'%d/%m/%Y') as allowedDate FROM paytv_subs " +
                " WHERE box_id=:qr AND package_id=:packageId AND SUBDATE(finish_on, INTERVAL 5 DAY)>NOW() ";

        List<Map<String,Object>> rows = jdt.queryForList(sql, params);
        if (rows.size()>0) {
            // there is an active product
            Map<String,Object> row = rows.get(0);
            String msgText = msgUtil.getMsgText(206);
            msgText = StringSubstitutor.replace(msgText, row);
            msgUtil.sendText(moMsg.getString("mobile"), msgText);
            refresh = true;
            endTime = new DateTime(msgUtil.parseDate(""+row.get("finish_on")));
        }

        log.debug("Begin time: "+beginTime+" / end time: "+endTime);

        String xmlTemplate = activityUtil.getCxXml("product.activation");
        String ref = UUID.randomUUID().toString();

        Map<String,String> cxParams = new HashMap<>();
        JSONObject jsonToken = activityUtil.generateCxToken();
        cxParams.put("token", jsonToken.getString("token"));
        cxParams.put("utctime", jsonToken.getString("utctime"));
        cxParams.put("ref", ref);
        cxParams.put("chipId", ""+stb.getLong("chip_id"));
        cxParams.put("beginTime", beginTime.toString("yyyy-MM-dd HH:mm:ss"));
        cxParams.put("endTime", endTime.toString("yyyy-MM-dd 00:00:00"));
        cxParams.put("packageId", product.getString("param1"));

        StringSubstitutor substitutor = new StringSubstitutor(cxParams);
        String xmlReq = substitutor.replace(xmlTemplate);

        JSONObject jsonResult = httpUtil.postToCx(xmlReq);

        boolean success = jsonResult.getJSONObject("Response").getJSONObject("Header").getBoolean("Success");

        Map<String,String> replacement = new HashMap<>();
        replacement.put("productName", product.getString("name"));
        replacement.put("endDate", endTime.minusDays(1).toString("dd/MM/yyyy"));

        if (success) {
            log.info("Activation "+product.get("name")+" to box "+stb.get("qr")+" is success!");

            if (refresh) return;
            //activityUtil.gotoNextActivity(sessionId, activityId, nextActivityId);

            params = new MapSqlParameterSource();
            params.addValues(cxParams);
            params.addValue("memberId", memberId);
            params.addValue("qr", stb.get("qr"));
            params.addValue("channelId", APP_CHANNEL_ID);
            params.addValue("productId", product.get("product_id"));

            sql = " INSERT INTO paytv_subs (member_id, channel_id,product_id,box_id,package_id,chip_id,ref_id,start_on,finish_on) " +
                    " VALUES (:memberId,:channelId,:productId,:qr,:packageId,:chipId,:ref,:beginTime,:endTime) " +
                    " ON DUPLICATE KEY UPDATE channel_id=:channelId, finish_on=:endTime, active_count=active_count+1 ";
            jdt.update(sql, params);

            msgUtil.sendImage(moMsg.getString("mobile"), StringSubstitutor.replace(msgUtil.getMsgText(209), replacement), product.getString("image_url"));
        } else {
            log.info("Activation "+product.get("name")+" to box "+stb.get("qr")+" is FAILED!");
            msgUtil.sendText(moMsg.getString("mobile"), StringSubstitutor.replace(msgUtil.getMsgText(210), replacement));
        }
    }

    private void refresh() {

    }
}
