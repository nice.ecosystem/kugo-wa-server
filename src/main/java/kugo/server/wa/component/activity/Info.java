package kugo.server.wa.component.activity;

import kugo.server.wa.component.HttpUtil;
import kugo.server.wa.component.MsgUtil;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

@Component
public class Info extends BaseActivity {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MsgUtil msgUtil;
    @Autowired
    ApplicationContext context;
    @Autowired
    ActivityUtil activityUtil;
    @Autowired
    HttpUtil httpUtil;

    public void init(JSONObject moMsg) throws Exception {
        log.info("Init Info activity! request: "+moMsg);
        parseMoMsg(moMsg);
        String replyText = msgUtil.getMsgText(201);
        msgUtil.sendText(moMsg.getString("mobile"), replyText);
    }

    public void validateStb(JSONObject moMsg) throws Exception {
        log.info("Validate STB Info activity! request: "+moMsg);
        parseMoMsg(moMsg);

        JSONObject stb = null;
        try {
            stb = activityUtil.validateStb(keyword, memberId);
        } catch (Exception e1) {
            try {
                stb = activityUtil.validateStb(keyword);
                msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(301));
                return;
            } catch (Exception e2) {
                msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(302));
                return;
            }
        }

        if (stb == null) {
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(302));
            return;
        }

        JSONArray activeProducts = getActiveProducts(stb);
        if (activeProducts.length()<=0) {
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(303));
            return;
        }

        String replyMsg = "STB # *"+stb.getString("qr")+"* Anda telah aktif, dan memiliki Paket tayangan berikut:\n";
        for (int i=0;i<activeProducts.length();i++) {
            JSONObject product = activeProducts.getJSONObject(i);
            DateTime endTime = dateTimeFormatterM.parseDateTime(product.getString("finish_on"));
            replyMsg+=(i+1)+". *"+product.getString("name")+"*, aktif s/d *"+endTime.toString("dd/MM/yyyy")+"*\n";
        }
        replyMsg = replyMsg.trim();
        msgUtil.sendText(moMsg.getString("mobile"), replyMsg);
        activityUtil.finishTheSession(sessionId);

    }

    private JSONArray getActiveProducts(JSONObject stb) {
        String qr = stb.getString("qr");
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("qr", qr);
        String sql = " SELECT p.name, s.* FROM paytv_subs s JOIN wallet_product p ON (p.product_id=s.product_id) " +
                " WHERE s.box_id=:qr AND finish_on>NOW() ORDER BY finish_on, mod_on DESC LIMIT 0,20 ";
        List<Map<String,Object>> rows = jdt.queryForList(sql, params);
        return new JSONArray(rows);
    }
}
