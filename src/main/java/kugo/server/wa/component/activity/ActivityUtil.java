package kugo.server.wa.component.activity;

import kugo.server.wa.component.MsgUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Random;

@Component
public class ActivityUtil {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    private NamedParameterJdbcTemplate jdt;
    @Autowired
    private MsgUtil msgUtil;
    @Autowired
    private ApplicationContext context;

    public void gotoNextActivity(Long sessionId, Long activityId, Long nextActivityId) throws Exception {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("sessionId", sessionId);
        params.addValue("activityId", activityId);
        params.addValue("nextActivityId", nextActivityId);

        String sql = " UPDATE wa_msg_session SET activity_id=:activityId, next_activity_id=:nextActivityId WHERE id=:sessionId ";
        jdt.update(sql, params);
    }

    public void updateSessionParam(Long sessionId, int number, Object paramValue) {

        log.info("Updating session paramater, session #"+sessionId+", param"+number+", value: "+paramValue);
        MapSqlParameterSource params = new MapSqlParameterSource();
        String paramName = "param"+number;
        params.addValue("sessionId", sessionId);
        params.addValue("paramValue", ""+paramValue);
        String sql = " UPDATE wa_msg_session SET "+paramName+"=:paramValue WHERE id=:sessionId ";
        jdt.update(sql, params);
    }

    public void updateMoText(Long sessionId, String keyword, String text) throws Exception {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("sessionId", sessionId);
        params.addValue("keyword", keyword);
        params.addValue("text", text);
        String sql = " UPDATE wa_msg_session SET last_keyword=:keyword, last_text=:text WHERE id=:sessionId ";
        jdt.update(sql, params);
    }

    public String getCxXml(String type) throws Exception {
        return IOUtils.toString(ActivityUtil.class.getResourceAsStream("/cx."+type+".xml"), StandardCharsets.UTF_8);
    }

    public JSONObject generateCxToken() {
        String sql = " SELECT DATE_FORMAT(UTC_TIMESTAMP(),'%Y-%m-%d %H:%i:%s') as utctime, SHA2(CONCAT('ninwa','ninwa',UTC_TIMESTAMP()),256) as token ";
        Map<String,Object> row = jdt.queryForMap(sql, new MapSqlParameterSource());
        return new JSONObject(row);
    }

    public int random(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public JSONObject getStbDetail(long chipId, String qr) {
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("qr", qr);
            params.addValue("chipId", chipId);

            String sql = " SELECT * FROM nice_cx.stb WHERE chip_id=:chipId OR qr=:qr LIMIT 0,1 ";
            Map<String,Object> row = jdt.queryForMap(sql, params);
            log.info("STB found, #"+row.get("stb_id"));
            return new JSONObject(row);
        } catch (Exception e) {
            return null;
        }
    }

    public JSONObject getStbDetail(long chipId) {
        return getStbDetail(chipId, "xxx");
    }

    public JSONObject getStbDetail(String qr) {
        return getStbDetail(0, qr);
    }

    public void finishTheSession(long sessionId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("sessionId", sessionId);
        String sql = " UPDATE wa_msg_session SET completed=1 WHERE id=:sessionId ";
        jdt.update(sql, params);
    }

    public JSONObject validateStb(String keyword, Long memberId) throws Exception {
        JSONObject stb = getStbDetail(keyword);

        if (stb==null && StringUtils.isNumeric(keyword) && keyword.length()>=8) {
            log.info("Chip ID: "+keyword);
            long chipId = Long.parseLong(keyword);
            stb = getStbDetail(chipId);
        }
        else if (stb==null && NumberUtils.isCreatable("0x"+keyword)) {
            log.info("Chip ID (HEX): "+keyword);
            long chipId = Long.parseLong(keyword,16);
            stb = getStbDetail(chipId);
            log.info("Chip ID: "+chipId);
        }

        if (stb == null) throw new Exception("STB not found!");
        String qr = stb.getString("qr");
        if (memberId!=null) {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("memberId", memberId);
            params.addValue("boxId", qr);

            String sql = " SELECT COUNT(*) as ct FROM paired_box WHERE box_id=:boxId and member_id=:memberId AND is_active=1 ";
            int count = jdt.queryForObject(sql,params,Integer.class);
            if (count<=0) throw new Exception("STB not found!");
        }

        return stb;
    }

    public JSONObject validateStb(String keyword) throws Exception {
        return validateStb(keyword, null);
    }
}
