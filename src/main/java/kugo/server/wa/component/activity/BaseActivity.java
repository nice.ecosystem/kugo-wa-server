package kugo.server.wa.component.activity;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;

public abstract class BaseActivity {
    Long activityId;
    Long nextActivityId;
    Long sessionId;
    Long memberId;
    String keyword;

    String param1;
    String param2;
    String param3;
    String param4;
    String param5;

    final static int APP_CHANNEL_ID = 3;
    final static DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    final static DateTimeFormatter dateTimeFormatterM = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.S");


    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    void parseMoMsg(JSONObject moMsg) throws Exception {
        keyword = moMsg.getString("keyword");
        activityId = moMsg.getLong("activityId");
        nextActivityId = (moMsg.has("nextActivityId"))?moMsg.getLong("nextActivityId"):null;
        sessionId = moMsg.getLong("sessionId");
        memberId = moMsg.getLong("memberId");

        param1 = (moMsg.has("param1"))?moMsg.getString("param1"):null;
        param2 = (moMsg.has("param2"))?moMsg.getString("param2"):null;
        param3 = (moMsg.has("param3"))?moMsg.getString("param3"):null;
        param4 = (moMsg.has("param4"))?moMsg.getString("param4"):null;
        param5 = (moMsg.has("param5"))?moMsg.getString("param5"):null;
    }

}
