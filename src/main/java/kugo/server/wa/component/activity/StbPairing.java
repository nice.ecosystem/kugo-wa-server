package kugo.server.wa.component.activity;

import kugo.server.wa.component.HttpUtil;
import kugo.server.wa.component.MsgUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.text.StringSubstitutor;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Component("stbPairing")
public class StbPairing extends BaseActivity {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    @Autowired
    NamedParameterJdbcTemplate jdt;
    @Autowired
    MsgUtil msgUtil;
    @Autowired
    ApplicationContext context;
    @Autowired
    ActivityUtil activityUtil;
    @Autowired
    HttpUtil httpUtil;

    private JSONObject moMsg;

    public void init(JSONObject moMsg) throws Exception {
        log.info("Init STB pairing! request: "+moMsg);
        this.moMsg = moMsg;
        String replyText = msgUtil.getMsgText(101);
        msgUtil.sendText(moMsg.getString("mobile"), replyText);
    }

    public void validateStb(JSONObject moMsg) throws Exception {
        log.info("Validating STB! request: "+moMsg);
        this.moMsg = moMsg;

        parseMoMsg(moMsg);

        String moText = moMsg.getString("msgText");

        activityUtil.updateMoText(sessionId, keyword, moText);

        try {

            JSONObject stb = activityUtil.validateStb(keyword);

            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(102));

            activityUtil.updateSessionParam(sessionId, 1, stb.getLong("chip_id"));
            activityUtil.gotoNextActivity(sessionId, activityId, nextActivityId);
        } catch (Exception e) {
            log.error("STB not found!");
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(103));
        }
    }

    public void sendValidationCode(JSONObject moMsg) throws Exception {
        log.info("Confirmation send code: "+moMsg);
        this.moMsg = moMsg;

        parseMoMsg(moMsg);

        if (!keyword.equals("YA")) {
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(105));
            return;
        }
        // send OTP
        String xmlTemplate = activityUtil.getCxXml("osd");
        String ref = UUID.randomUUID().toString();

        int code = activityUtil.random(1111,9999);
        activityUtil.updateSessionParam(sessionId, 2, ref);
        activityUtil.updateSessionParam(sessionId, 3, code);

        Map<String,String> cxParams = new HashMap<>();
        JSONObject jsonToken = activityUtil.generateCxToken();
        cxParams.put("token", jsonToken.getString("token"));
        cxParams.put("utctime", jsonToken.getString("utctime"));
        cxParams.put("ref", ref);
        cxParams.put("chipId", moMsg.getString("param1"));
        cxParams.put("text", ""+code);

        StringSubstitutor substitutor = new StringSubstitutor(cxParams);
        String xmlReq = substitutor.replace(xmlTemplate);
        log.info("XML request: \n"+xmlReq);
        //msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(109));

        JSONObject jsonResult = httpUtil.postToCx(xmlReq);

        boolean success = jsonResult.getJSONObject("Response").getJSONObject("Header").getBoolean("Success");
        if (success) {
            activityUtil.gotoNextActivity(sessionId, activityId, nextActivityId);
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(104));
        } else {
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(106));
        }

        //activityUtil.gotoNextActivity(sessionId, activityId, nextActivityId);
    }

    public void validateCode(JSONObject moMsg) throws Exception {
        log.info("Validating code: "+moMsg);
        this.moMsg = moMsg;

        String pin = moMsg.getString("param3");
        String ref = moMsg.getString("param2");
        long chipId = Long.parseLong(moMsg.getString("param1"));

        parseMoMsg(moMsg);

        JSONObject stb = activityUtil.getStbDetail(chipId);

        String keyword = moMsg.getString("keyword");

        if (!pin.equals(keyword)) {
            msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(107));
            return;
        }
        log.info("PIN is valid! Pairing the box to the member!");

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("qr", stb.getString("qr"));
        params.addValue("chipId", ""+chipId);
        params.addValue("reqId", ref);
        params.addValue("memberId", memberId);
        params.addValue("channelId", APP_CHANNEL_ID);

        String sql = " UPDATE paired_box SET is_active=0 WHERE box_id=:qr ";
        jdt.update(sql, params);

        sql = "  INSERT INTO paired_box (channel_id,member_id,box_id,box_alias,subs_id,is_active,req_id) VALUES (:channelId,:memberId,:qr,:qr,:chipId,1,:reqId) ";
        jdt.update(sql, params);

        activityUtil.finishTheSession(sessionId);

        msgUtil.sendText(moMsg.getString("mobile"), msgUtil.getMsgText(108));
    }
}
